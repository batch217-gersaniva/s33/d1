// console.log("Hello World!");

// [SECTION] -JavaScript Synchronous vs Asynchronous
//JS --> Synchronous - it can execute one statement or one loine of code at a time.
// read from top to bottom, left o right.


console.log("Hello World!");
// conosle.log("Hello Again!");
console.log("Goodbye");


// Asynchronous - program will continue reading the next line if there

console.log("Hello World!");
// for (let i = 0; i <= 1500; i++){
// 	console.log(i);
// }
console.log("Hello Again!");


// [SECTION] Getting all post
//fetch() - fetch request
//The fetch API allows programmers to asynchronously request for a resource (data).

/*
	Syntax --- //fetch()
	console.log('fetch(https://jsonplaceholder.typicode.com/posts'))

*/ 

fetch("https://jsonplaceholder.typicode.com/posts")
//We use "json" method from the response object to covert datainto JSON format
.then((response) => response.json())
.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous
//Used in functions to indicate which portions of the code should be awaited for.

async function fetchData(){    //fetchData can be replaced with any name

	// wait for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	// result retured "response" -- expecting a promise
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json      //let json ==> variable
	console.log(json);
}

fetchData();

//[SECTION] Getting a specific post 
fetch ("https://jsonplaceholder.typicode.com/posts/1")
// every .then indicates a promise
.then((response) => response.json())
.then((json) => console.log(json))

//[SECTION] Creating a post

/*

Syntax:

fetch("URL", options)
.then((response) => {})
.then((response) => {})

*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST", 
	header: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New Post",	 //title, body, userID are property
		body: "Hello World",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//[SECTION] Updating a post
//PUT Method
//whole content of post will be modified

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again!",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Patch method
//almost same with PUT method
//Used to update a specific property in a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",       //can be method: "PUT"
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post",
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

Note: 

// PATCH - used to update a single/several properties
// PUT - used to update the whole document


// [SECTION] Deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});

//[SECTION] Filtering Post


/*

Syntax :
Individual Parameter
// 'url?parameterName=value'

Multiple Parameter
url?paramA=valueA&paramB=valueB

*/

fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response) => response.json())
.then((json) => console.log(json));




// [SECTION] Retrieving comments on a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json));


console.log(fetch("https://jsonplaceholder.typicode.com/posts"))